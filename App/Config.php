<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{        
    
    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'db751074527.db.1and1.com:3306';
    const DEV_DB_HOST = 'localhost:3306';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'db751074527';
    
    const DEV_DB_NAME = 'portfolio';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'dbo751074527';
    const DEV_DB_USER = 'root';
    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = 'flamengo!@3';
    const DEV_DB_PASSWORD = '';
    
    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const SHOW_ERRORS = true;
    
    /**
     * Show or hide error messages on screen
     * @var boolean
     */
    const DEBUG = true;

    /**
     * Secret key for hashing
     * @var boolean
     */
    const SECRET_KEY = 'your-secret-key';

    /**
     * Mailgun API key
     *
     * @var string
     */
    const MAILGUN_API_KEY = 'dcd68c2ebe953afe03013228dd3a2509-a4502f89-5015650f';

    /**
     * Mailgun domain
     *
     * @var string
     */
    const MAILGUN_DOMAIN = 'sandbox6a03c2bbf44a405abdbf029b9977b9cb.mailgun.org';
}
