<?php

namespace App\Controllers;

use \Core\View;
use \App\Flash;
use App\Models\CustomizationModel;
use Core\Controller;
use App\Auth;

/**
 * Customization controller
 *
 * PHP version 7.0
 */
class Customization extends Authenticated
{

    /**
     * Before filter - called before each action method
     *
     * @return void
     */
    protected function before()
    {
        parent::before();
        $this->user = Auth::getUser();
        $this->customization = CustomizationModel::findByUserID($this->user->id);
        

    }

    /**
     * Show the profile
     *
     * @return void
     */
    public function showAction()
    {
        View::renderTemplate('Customization/show.html', [
            'customization' => $this->customization,
        ]); 
    }

    /**
     * Show the form for editing the profile
     *
     * @return void
     */
    public function editAction()
    {
        $this->customizationModel = CustomizationModel::findById($this->route_params['id']);
        
        View::renderTemplate('Customization/edit.html', [
            'customization' => $this->customizationModel
        ]);
    }
    
    /**
     * Delete a user
     *
     * @return void
     */
    public function deleteAction()
    {   
        //confirming that the line exist
        $this->customizationModel = CustomizationModel::findByID($this->route_params['id']);
        
        if ($this->customizationModel->deleteCustomization($this->route_params['id'])) {

            Flash::addMessage('Customization deleted');

            $this->redirect('/customize/show');

        }
    }
    
    /**
     * Show the signup page
     *
     * @return void
     */
    public function newAction()
    {
        View::renderTemplate('Customization/new.html');
    }
    
    /**
     * Sign up a new user
     *
     * @return void
     */
    public function createAction()
    {
        $this->customizationModel = new CustomizationModel($_POST);
        
        $errorimg = $_FILES["image"]["error"];
        Controller::debug_to_console("4 errorimg: " . $errorimg);
        
        if($errorimg > 0){
           Controller::debug_to_console("An error occurred while uploading the file");
           Flash::addMessage('An error occurred while uploading the file');
        }
       
        if($_FILES["image"]["size"] > 5000000){
           Controller::debug_to_console("File is too big");
            die('<div class="alert alert-danger" role="alert"> File is too big </div>');
        }
        
        $valid_extensions = array('jpg', 'png', 'jpeg'); // valid extensions
        $dirpath = realpath(dirname(getcwd()));
        $path = 'img/'; // upload directory

        
        if($_FILES['image'])
        {
            
            $img = $_FILES['image']['name'];
            $tmp = $_FILES['image']['tmp_name'];
            
            Controller::debug_to_console("tmp: " . $tmp);
            
            $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));

            // can upload same image using rand function
//            $final_image = rand(1000,1000000).$img;
            
            if ($this->route_params['image'] != null 
                   && $this->route_params['image'] == 'profile'){
               $final_image = $this->user->id . "_profile.jpg";
            }
            if($this->route_params['image'] != null 
                   && $this->route_params['image'] == 'background'){
                $final_image = $this->user->id . "_background.jpg";
            }
           
            
            // check's valid format
            if(in_array($ext, $valid_extensions)) 
            { 
                $path = $path.strtolower($final_image); 
                
                if(file_exists(realpath($path))) {
                    Controller::debug_to_console("path exits: " . $path);
                    chmod(realpath($path),0755); //Change the file permissions if allowed
                   // unlink(realpath($path)); //remove the file
                }
                
                if(move_uploaded_file($tmp,$path)) 
                {
                    Controller::debug_to_console("move_uploaded_file OK");
                    $data = array(
                        "image" => $final_image,
                        "color" => "rgba(0, 0, 0, 0.38)",
                        "user_id" => $this->user->id,
                        "id" => $this->customization->id
                    );
                    
                    $this->customizationModel = new CustomizationModel($data);
                    if(isset($this->customization)){
                        $this->customizationModel->updateCustomization();
                    }else{
                        $this->customizationModel->save();
                    }
                    
                    
                    $this->redirect('/customization/show');
                }
            } 
            else 
            {
                Flash::addMessage("Invalid file...");
                $this->redirect('/customization/show');
            }
            
        }
//        if ($this->customizationModel->save()) {
//            Flash::addMessage('Changes saved');
//            //$user->sendActivationEmail();
//            $this->redirect('/customize/show');  
//        }else{
//            View::renderTemplate('Customization/new.html', [
//                'customization' => $this->customizationModel
//            ]);
//        }
    }

    /**
     * Update the profile
     *
     * @return void
     */
    public function updateAction()
    {
        //confirming that the line exist
        $this->customizationModel = CustomizationModel::findByID($this->route_params['id']);
        
        if ($this->customizationModel->updateCustomization($_POST)) {

            Flash::addMessage('Changes saved');

            $this->redirect('/customize/show');

        }else {

            View::renderTemplate('Customization/edit.html', [
                'customization' => $this->customizationModel
            ]);

        }
    }
    
}
