<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;

/**
 * Home controller
 *
 * PHP version 7.0
 */

use App\Models\CustomizationModel;

class Home extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {   
        $this->user = Auth::getUser();
        
        if (!isset($this->user)) {
            $this->name = 'dmaximo.com';
        }else{
            $this->name = $this->user->name;
        }
        
        View::renderTemplate('Home/index.html', ['name' => $this->name
        ]);
    }
}
