<?php

namespace App\Controllers;

use \Core\View;
use \App\Flash;
use App\Models\PropertyModel;
use Core\Controller;

/**
 * Property controller
 *
 * PHP version 7.0
 */
class Property extends Authenticated
{

    /**
     * Before filter - called before each action method
     *
     * @return void
     */
    protected function before()
    {
        parent::before();

    }

    /**
     * Show the profile
     *
     * @return void
     */
    public function showAction()
    {
        $this->properties = PropertyModel::getAll();
        
        View::renderTemplate('Property/show.html', [
            'properties' => $this->properties
        ]);
    }

    /**
     * Show the form for editing the profile
     *
     * @return void
     */
    public function editAction()
    {
        $this->propertyModel = PropertyModel::findById($this->route_params['id']);
        
        View::renderTemplate('Property/edit.html', [
            'property' => $this->propertyModel
        ]);
    }
    
    /**
     * Delete a user
     *
     * @return void
     */
    public function deleteAction()
    {   
        //confirming that the line exist
        $this->propertyModel = PropertyModel::findByID($this->route_params['id']);
        
        if ($this->propertyModel->deleteProperty($this->route_params['id'])) {

            Flash::addMessage('Property deleted');

            $this->redirect('/property/show');

        }
    }
    
    /**
     * Show the signup page
     *
     * @return void
     */
    public function newAction()
    {
        View::renderTemplate('Property/new.html');
    }
    
    /**
     * Sign up a new user
     *
     * @return void
     */
    public function createAction()
    {
        $this->propertyModel = new PropertyModel($_POST);
        
        if ($this->propertyModel->save()) {
            Flash::addMessage('Changes saved');
            //$user->sendActivationEmail();
            $this->redirect('/property/show');  
        }else{
            View::renderTemplate('Property/new.html', [
                'property' => $this->propertyModel
            ]);
        }
    }

    /**
     * Update the profile
     *
     * @return void
     */
    public function updateAction()
    {
        //confirming that the line exist
        $this->propertyModel = PropertyModel::findByID($this->route_params['id']);
        
        if ($this->propertyModel->updateProperty($_POST)) {

            Flash::addMessage('Changes saved');

            $this->redirect('/property/show');

        }else {

            View::renderTemplate('Property/edit.html', [
                'property' => $this->propertyModel
            ]);

        }
    }
}
