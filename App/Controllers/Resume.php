<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;


use \Core\View;
use \App\Flash;
use \App\Models\ResumeModel;
use \App\Models\ResumeSectionModel;
use \App\Models\ResumeSectionItemModel;
use \App\Auth;

/**
 * Description of Resume
 *
 * @author Maximo
 */
class Resume extends Authenticated{

    /**
     * Before filter - called before each action method
     *
     * @return void
     */
    protected function before()
    {
        parent::before();
        $this->user = Auth::getUser();
        
    }

    /**
     * Show the profile
     *
     * @return void
     */
    public function showAction()
    {
//        $this->resumes = ResumeModel::getAll();
        $this->versions = ResumeModel::getAllVersions(); 
        $this->resumes = ResumeModel::findByVersion($this->route_params['version']);
        $this->resumesections = ResumeSectionModel::getAll();
        $this->resumesectionitems = ResumeSectionItemModel::getAll();
        
        View::renderTemplate('resume/show.html', [
            'resumes' => $this->resumes,
            'versions' => $this->versions,
            'resumesections' => $this->resumesections,
            'resumesectionitems' => $this->resumesectionitems
        ]);
    }
    
    public function showDefaultVersionAction()
    {
        $this->redirect('/resume/v1');
    }
    

    /**
     * Show the form for editing the profile
     *
     * @return void
     */
    public function editAction()
    {
        $this->resumeModel = ResumeModel::findById($this->route_params['id']);
        View::renderTemplate('resume/edit.html', [
            'resume' => $this->resumeModel
        ]);
    }
    
    /**
     * Delete a user
     *
     * @return void
     */
    public function deleteAction()
    {   
        //confirming that the line exist
        $this->resumeModel = ResumeModel::findByID($this->route_params['id']);
        
        if ($this->resumeModel->deleteResume($this->route_params['id'])) {
            Flash::addMessage('Resume deleted');
            $this->redirect('/resume/v1');
        }
    }
    
    /**
     * Show the signup page
     *
     * @return void
     */
    public function newAction()
    {
        $this->maxversion = ResumeModel::getNextVersion();
            
        View::renderTemplate('resume/new.html', [
                'maxversion'     => $this->maxversion,
            ]);
    }
    
    /**
     * Sign up a new user
     *
     * @return void
     */
    public function createAction()
    {
        $this->resumeModel = new ResumeModel($_POST, $this->user->id);
        
        if ($this->resumeModel->save()) {
            Flash::addMessage('Changes saved');
            //$user->sendActivationEmail();
            $this->redirect('/resume/v' . $this->resumeModel->version);  
        }else{
            View::renderTemplate('resume/new.html', [
                'resume' => $this->resumeModel,
            ]);
        }
    }

    /**
     * Update the profile
     *
     * @return void
     */
    public function updateAction()
    {
        //confirming that the line exist
        $this->resumeModel = ResumeModel::findByID($this->route_params['id']);
        
        if ($this->resumeModel->updateResume($_POST)) {
            Flash::addMessage('Changes saved');
            $this->redirect('/resume/v' . $this->resumeModel->version);
        }else {
            View::renderTemplate('resume/edit.html', [
                'resume' => $this->resumeModel
            ]);
        }
    }
}
