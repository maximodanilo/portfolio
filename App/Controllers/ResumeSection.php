<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;


use \Core\View;
use \App\Flash;
use \App\Models\ResumeSectionModel;
use \App\Models\ResumeModel;
use \App\Auth;

/**
 * Description of Resume
 *
 * @author Maximo
 */
class ResumeSection extends Authenticated{

    /**
     * Before filter - called before each action method
     *
     * @return void
     */
    protected function before()
    {
        parent::before();
        $this->user = Auth::getUser();
        
    }

    /**
     * Show the profile
     *
     * @return void
     */
    public function showAction()
    {
        $this->resumesections = ResumeSectionModel::getAll();
        
        View::renderTemplate('resumesection/show.html', [
            'resumesections' => $this->resumesections  
        ]);
    }

    /**
     * Show the form for editing the profile
     *
     * @return void
     */
    public function editAction()
    {
        $this->resumeSectionModel = ResumeSectionModel::findById($this->route_params['id']);
        $this->resumeVersion = ResumeModel::getVersionByID($this->resumeSectionModel->resume_id);
        View::renderTemplate('resumesection/edit.html', [
            'resumesection' => $this->resumeSectionModel,
            'resumeversion' => $this->resumeVersion
        ]);
    }
    
    /**
     * Delete a user
     *
     * @return void
     */
    public function deleteAction()
    {   
        //confirming that the line exist
        $this->resumeSectionModel = ResumeSectionModel::findByID($this->route_params['id']);
        
        $this->resumeVersion = ResumeModel::getVersionByID($this->resumeSectionModel->resume_id);

        if ($this->resumeSectionModel->deleteResumeSection($this->route_params['id'])) {
            Flash::addMessage('Resume Section deleted');
            $this->redirect('/resume/v' . $this->resumeVersion);
        }
    }
    
    /**
     * Show the signup page
     *
     * @return void
     */
    public function newAction()
    {
        
        $this->resumeVersion = ResumeModel::getVersionByID($this->route_params['resumeid']);
        
        View::renderTemplate('resumesection/new.html', [
                'resumeid' => $this->route_params['resumeid'],
                'resumeversion' => $this->resumeVersion
            ]);
    }
    
    /**
     * Sign up a new user
     *
     * @return void
     */
    public function createAction()
    {
        $this->resumeSectionModel = new ResumeSectionModel($_POST);
        $this->resumeSectionModel->resume_id = $this->route_params['resumeid'];
        
        $this->resumeVersion = ResumeModel::getVersionByID($this->resumeSectionModel->resume_id);
        
        if ($this->resumeSectionModel->save()) {
            Flash::addMessage('Changes saved');
            //$user->sendActivationEmail();
            $this->redirect('/resume/v' . $this->resumeVersion);  
        }else{
            View::renderTemplate('resumesection/new.html', [
                'resumesection' => $this->resumeSectionModel,
                'resumeversion' => $this->resumeVersion
            ]);
        }
    }

    /**
     * Update the profile
     *
     * @return void
     */
    public function updateAction()
    {
        //confirming that the line exist
        $this->resumeSectionModel = ResumeSectionModel::findByID($this->route_params['id']);
        
        $this->resumeVersion = ResumeModel::getVersionByID($this->resumeSectionModel->resume_id, 'version');
        
        echo  $this->resumeVersion;
        
        if ($this->resumeSectionModel->updateResumeSection($_POST)) {
            Flash::addMessage('Changes saved');
            $this->redirect('/resume/v' . $this->resumeVersion);
        }else {
            View::renderTemplate('resumesection/edit.html', [
                'resumesection' => $this->resumeSectionModel
            ]);
        }
    }
}
