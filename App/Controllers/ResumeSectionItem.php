<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;


use \Core\View;
use \App\Flash;
use \App\Models\ResumeSectionItemModel;
use \App\Models\ResumeModel;
use \App\Auth;

/**
 * Description of Resume
 *
 * @author Maximo
 */
class ResumeSectionItem extends Authenticated{

    /**
     * Before filter - called before each action method
     *
     * @return void
     */
    protected function before()
    {
        parent::before();
        $this->user = Auth::getUser();
        
    }

    /**
     * Show the profile
     *
     * @return void
     */
    public function showAction()
    {
        $this->resumesectionitems = ResumeSectionItemModel::getAll();
        View::renderTemplate('resumesectionitem/show.html', [
            'resumesectionitems' => $this->resumesectionitems  
        ]);
    }

    /**
     * Show the form for editing the profile
     *
     * @return void
     */
    public function editAction()
    {
        $this->resumeSectionItemModel = ResumeSectionItemModel::findById($this->route_params['id']);
        $this->resumeVersion = ResumeModel::getVersionBySectionItemID($this->resumeSectionItemModel->id);
        
        View::renderTemplate('resumesectionitem/edit.html', [
            'resumesectionitem' => $this->resumeSectionItemModel, 
            'resumeversion' => $this->resumeVersion
        ]);
    }
    
    /**
     * Delete a user
     *
     * @return void
     */
    public function deleteAction()
    {   
        //confirming that the line exist
        $this->resumeSectionItemModel = ResumeSectionItemModel::findByID($this->route_params['id']);
        $this->resumeVersion = ResumeModel::getVersionBySectionItemID($this->resumeSectionItemModel->id);
        
        if ($this->resumeSectionItemModel->deleteResumeSectionItem($this->route_params['id'])) {
            Flash::addMessage('Resume Section Item deleted');
            $this->redirect('/resume/v' . $this->resumeVersion);
        }
    }
    
    /**
     * Show the signup page
     *
     * @return void
     */
    public function newAction()
    {
        $this->resumeVersion = ResumeModel::getVersionByID($this->route_params['resumeid']);
        
        View::renderTemplate('resumesectionitem/new.html', [
                'resumesectionid' => $this->route_params['resumesectionid'],
                'resumeid' => $this->route_params['resumeid'],
                'resumeversion' => $this->resumeVersion
            ]);
    }
    
    /**
     * Sign up a new user
     *
     * @return void
     */
    public function createAction()
    {
        $this->resumeSectionItemModel = new ResumeSectionItemModel($_POST);
        $this->resumeSectionItemModel->res_section_id = $this->route_params['resumesectionid'];
        $this->resumeVersion = ResumeModel::getVersionByID($this->route_params['resumeid']);
        
        if ($this->resumeSectionItemModel->save()) {
            Flash::addMessage('Changes saved');
            //$user->sendActivationEmail();
            $this->redirect('/resume/v' . $this->resumeVersion);  
        }else{
            View::renderTemplate('resumesectionitem/new.html', [
                'resumesectionitem' => $this->resumeSectionItemModel,
                'resumeversion' => $this->resumeVersion
            ]);
        }
    }

    /**
     * Update the profile
     *
     * @return void
     */
    public function updateAction()
    {
        //confirming that the line exist
        $this->resumeSectionItemModel = ResumeSectionItemModel::findByID($this->route_params['id']);
        
        $this->resumeVersion = ResumeModel::getVersionBySectionItemID($this->resumeSectionItemModel->id);
        
        if ($this->resumeSectionItemModel->updateResumeSectionItem($_POST)) {
            Flash::addMessage('Changes saved');
            $this->redirect('/resume/v' . $this->resumeVersion);
        }else {
            View::renderTemplate('resumesectionitem/edit.html', [
                'resumesectionitem' => $this->resumeSectionItemModel,
                'resumeversion' => $this->resumeVersion
            ]);
        }
    }
}
