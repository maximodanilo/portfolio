<?php

namespace App\Models;

use PDO;

/**
 * Customization model
 *
 * PHP version 7.0
 */
class CustomizationModel extends \Core\Model {

    /**
     * Error messages
     *
     * @var array
     */
    public $errors = [];

    /**
     * Class constructor
     *
     * @param array $data  Initial customization values (optional)
     *
     * @return void
     */
    public function __construct($data = []) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
            \Core\Controller::debug_to_console($key);
            \Core\Controller::debug_to_console($value);
        };
    }

    /**
     * Save the user model with the current customization values
     *
     * @return boolean  True if the user was saved, false otherwise
     */
    public function save() {
//        $this->validate();

        if (empty($this->errors)) {

            $sql = 'INSERT INTO customization (user_id, color, image)
                    VALUES (:user_id, :color, :image)';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':user_id', $this->user_id, PDO::PARAM_INT);
            $stmt->bindValue(':color', $this->color, PDO::PARAM_STR);
            $stmt->bindValue(':image', $this->image, PDO::PARAM_STR);

            return $stmt->execute();
        }

        return false;
    }
    
     /**
     * Delete the user model
     *
     * @return boolean  True if the user was delete, false otherwise
     */
    public function deleteCustomization($id) {

        if (empty($this->errors)) {

            $sql = 'DELETE from customization where id = :id';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
           
            return $stmt->execute();
        }

        return false;
    }
    
        /**
     * Update the user's customization
     *
     * @param array $data Data from the edit customization form
     *
     * @return boolean  True if the data was updated, false otherwise
     */
    public function updateCustomization() {
        
//        $this->color = $data['color'];
//        $this->image = $data['image'];

//        $this->validate();

        if (empty($this->errors)) {

            $sql = 'UPDATE customization
                    SET color = :color,
                        image = :image
                    WHERE id = :id';
            
            \Core\Controller::debug_to_console($sql);

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':color', $this->color, PDO::PARAM_STR);
            $stmt->bindValue(':image', $this->image, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }

     /**
     * Return all properties in the database
     *
     * @param none
     *
     * @return Properties array if any, false otherwise
     */
    public static function getAll() {
        $sql = 'SELECT * FROM customization';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        //$stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetchAll();
    }
    
    /**
     * Find a customization model by ID
     *
     * @param string $id The customization ID
     *
     * @return mixed Customization object if found, false otherwise
     */
    public static function findByID($id) {
        $sql = 'SELECT * FROM customization WHERE id = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function findByUserID($id) {
        $sql = 'SELECT * FROM customization WHERE user_id = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }
    /**
     * Validate current customization values, adding validation error messages to the errors array customization
     *
     * @return void
     */
    public function validate() {
        // Name
        if (isset($this->description)) {

            if ($this->name == '') {
                $this->errors[] = 'Name is required';
                \Core\Controller::debug_to_console("name is required");
            }
            // name
            if (strlen($this->name) < 5) {
                $this->errors[] = 'Please enter at least 5 characters for the name';
            }

            if (strlen($this->name) > 50) {
                $this->errors[] = 'Please enter at most 50 characters for the name';
            }
        }
        
        // description
        if (isset($this->description)) {
            if ($this->description == '') {
                $this->errors[] = 'Description is required';
                \Core\Controller::debug_to_console("description is required");
            }
            if (strlen($this->description) < 10) {
                $this->errors[] = 'Please enter at least 10 characters for the description';
            }
            
            if (strlen($this->description) > 2000) {
                $this->errors[] = 'Please enter at most 2000 characters for the description';
            }


        }
    }

}
