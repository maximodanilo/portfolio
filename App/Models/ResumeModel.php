<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use PDO;

/**
 * Description of ResumeModel
 *
 * @author Maximo
 */
class ResumeModel extends \Core\Model {

    public $errors = [];

    /**
     * Class constructor
     *
     * @param array $data  Initial resume values (optional)
     *
     * @return void
     */
    public function __construct($data = []) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
            \Core\Controller::debug_to_console($key);
            \Core\Controller::debug_to_console($value);
        };
    }

    /**
     * Save the user model with the current resume values
     *
     * @return boolean  True if the user was saved, false otherwise
     */
    public function save() {
       
        $this->validate();

        if (empty($this->errors)) {

            $db = static::getDB();
            $sqlResume = 'INSERT INTO resume (user_id, version, visible)
                    VALUES (:user_id, :version, :visible)';

            $stmt = $db->prepare($sqlResume);
            $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
            $stmt->bindValue(':version', $this->version, PDO::PARAM_INT);
            $stmt->bindValue(':visible', $this->visible, PDO::PARAM_STR);

            return $stmt->execute();
        }

        return false;
    }

    /**
     * Delete the user model
     *
     * @return boolean  True if the user was delete, false otherwise
     */
    public function deleteResume($id) {

        if (empty($this->errors)) {

            $sql = 'DELETE from resume '
                    . 'WHERE id = :id '
                    . 'and user_id = :user_id';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);

            return $stmt->execute();
        }

        return false;
    }

    /**
     * Update the user's resume
     *
     * @param array $data Data from the edit resume form
     *
     * @return boolean  True if the data was updated, false otherwise
     */
    public function updateResume($data) {

        $this->visible = $data['visible'];
        $this->version = $data['version'];
        
        $this->validate();

        if (empty($this->errors)) {

            $sql = 'UPDATE resume
                    SET   visible = :visible
                        , version = :version
                    WHERE id = :id
                      and user_id = :user_id';


            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':visible', $this->visible, PDO::PARAM_STR);
            $stmt->bindValue(':version', $this->version, PDO::PARAM_INT);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);

            $v_return = $stmt->execute();
            
            if($v_return) {
                return $this->visibilityControl();
            }
            
        }
        return false;
    }
    
    public function visibilityControl() {

        //$this->visible = $data['visible'];
        
        if($this->visible == 'YES'){
            $sql = 'UPDATE resume
                    SET   visible = :visible
                    WHERE id <> :id
                      and user_id = :user_id';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':visible', 'NO', PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);

            return $stmt->execute();
        }else{
            return true;
        }
       
        return false;
    }

    /**
     * Return all properties in the database
     *
     * @param none
     *
     * @return Properties array if any, false otherwise
     */
    public static function getAll() {
//        $sql = 'SELECT * FROM resume_vw';
        $sql = 'SELECT * FROM resume where user_id = :user_id';
        
        $db = static::getDB();
        $stmt = $db->prepare($sql);
        //$stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());
        $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetchAll();
    }
    
    public static function getAllVersions() {
//        $sql = 'SELECT * FROM resume_vw';
        $sql = 'SELECT version FROM resume where user_id = :user_id';
        
        $db = static::getDB();
        $stmt = $db->prepare($sql);
        //$stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_COLUMN, 0);
        $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetchAll();
    }
    
        public static function getNextVersion() {
//        $sql = 'SELECT * FROM resume_vw';
        $sql = 'SELECT max(version) maxversion FROM resume where user_id = :user_id';
        
        $db = static::getDB();
        $stmt = $db->prepare($sql);
        //$stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_COLUMN, 0);
        $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * Find a resume model by ID
     *
     * @param string $id The resume_section ID
     *
     * @return mixed Resume object if found, false otherwise
     */
    public static function findByID($id) {
        $sql = 'SELECT * FROM resume '
                . 'WHERE id = :id '
                . 'and user_id = :user_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
        
        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }
    
     public static function getVersionByID($id) {
        $sql = 'SELECT version FROM resume '
                . 'WHERE id = :id '
                . 'and user_id = :user_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
//        $stmt->bindValue(':field', $field, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
        
        $stmt->setFetchMode(PDO::FETCH_COLUMN, 0);

        $stmt->execute();

        return $stmt->fetch();
    }
    
     public static function getVersionBySectionItemID($resSecItemID) {
        $sql = 'SELECT res_version FROM resume_vw '
                . 'WHERE res_sec_item_id = :res_sec_item_id '
                . 'and user_id = :user_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':res_sec_item_id', $resSecItemID, PDO::PARAM_INT);
        $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
        
        $stmt->setFetchMode(PDO::FETCH_COLUMN, 0);

        $stmt->execute();

        return $stmt->fetch();
    }
    
    /**
     * Find a resume model by ID
     *
     * @param string $id The resume_section ID
     *
     * @return mixed Resume object if found, false otherwise
     */
    public static function findByVersion($version) {
        
        $version = str_replace('v', '', $version);
        
        $sql = 'SELECT * FROM resume '
                . 'WHERE version = :version'
                . ' and user_id = :user_id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':version', str_replace('v', '', $version), PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
        
        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Validate current resume values, adding validation error messages to the errors array resume
     *
     * @return void
     */
    public function validate() {
        // visible
        if (isset($this->visible)) {

            if ($this->visible == '') {
                $this->errors[] = 'Visibility is required';
                \Core\Controller::debug_to_console("visible is required");
            }
            // visible
            if (!$this->visible === 'YES' && !$this->visible === 'NO') {
                $this->errors[] = 'Please enter YES or NO';
            }

        }
        
        // version
        if (isset($this->version)) {

            if ($this->version == '') {
                $this->errors[] = 'Version is required';
                \Core\Controller::debug_to_console("Version is required");
            }
            
            $maxversion = self::getNextVersion();
            
            \Core\Controller::debug_to_console("Version is required " . ($maxversion) . " " . $this->version ); 
            
            // Version
            if ($this->version <= $maxversion && !isset($this->id)) {
                $this->errors[] = 'This version already exists';
            }

        }
        
//
//        // description
//        if (isset($this->description)) {
//            if ($this->description == '') {
//                $this->errors[] = 'Description is required';
//                \Core\Controller::debug_to_console("description is required");
//            }
//            if (strlen($this->description) < 10) {
//                $this->errors[] = 'Please enter at least 10 characters for the description';
//            }
//
//            if (strlen($this->description) > 2000) {
//                $this->errors[] = 'Please enter at most 2000 characters for the description';
//            }
//        }
    }

}
