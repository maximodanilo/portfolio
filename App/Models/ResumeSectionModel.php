<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use PDO;

/**
 * Description of ResumeSectionModel
 *
 * @author Maximo
 */
class ResumeSectionModel extends \Core\Model {

    public $errors = [];

    /**
     * Class constructor
     *
     * @param array $data  Initial resume values (optional)
     *
     * @return void
     */
    public function __construct($data = []) {
        foreach ($data as $key => $value) {
            $this->$key = $value;
            \Core\Controller::debug_to_console($key);
            \Core\Controller::debug_to_console($value);
        };
    }

    /**
     * Save the user model with the current resume values
     *
     * @return boolean  True if the user was saved, false otherwise
     */
    public function save() {
        $this->validate();

        if (empty($this->errors)) {
            $db = static::getDB();


            $sqlResume = 'INSERT INTO resume_section (resume_id, name)
                    VALUES (:resume_id, :name)';

            $stmt = $db->prepare($sqlResume);
            $stmt->bindValue(':resume_id', $this->resume_id, PDO::PARAM_INT);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);

            return $stmt->execute();
        }

        return false;
    }

    /**
     * Delete the user model
     *
     * @return boolean  True if the user was delete, false otherwise
     */
    public function deleteResumeSection($id) {

        if (empty($this->errors)) {

            $sql = 'DELETE from resume_section where id = :id';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            return $stmt->execute();
        }

        return false;
    }

    /**
     * Update the user's resume
     *
     * @param array $data Data from the edit resume form
     *
     * @return boolean  True if the data was updated, false otherwise
     */
    public function updateResumeSection($data) {

        $this->name = $data['name'];
        
        $this->validate();

        if (empty($this->errors)) {

            $sql = 'UPDATE resume_section
                    SET name = :name
                    WHERE id = :id';

            \Core\Controller::debug_to_console($sql);

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':name', $data['name'], PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);

            return $stmt->execute();
        }
        return false;
    }

    /**
     * Return all properties in the database
     *
     * @param none
     *
     * @return Properties array if any, false otherwise
     */
    public static function getAll() {
        $sql = 'SELECT * FROM resume_section';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        //$stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Find a resume model by ID
     *
     * @param string $id The resume_section ID
     *
     * @return mixed Resume object if found, false otherwise
     */
    public static function findByID($id) {
        $sql = 'SELECT * FROM resume_section WHERE id = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * Validate current resume values, adding validation error messages to the errors array resume
     *
     * @return void
     */
    public function validate() {
        // Name
        if (isset($this->name)) {

            if ($this->name == '') {
                $this->errors[] = 'Name is required';
                \Core\Controller::debug_to_console("name is required");
            }
            // name
            if (strlen($this->name) < 1) {
                $this->errors[] = 'Please enter at least 1 characters for the name';
            }

            if (strlen($this->name) > 50) {
                $this->errors[] = 'Please enter at most 50 characters for the name';
            }
        }
    }

}
