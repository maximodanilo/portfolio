<?php

namespace Core;

use PDO;
use App\Config;

/**
 * Base model
 *
 * PHP version 7.0
 */
abstract class Model
{

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    protected static function getDB()
    {
        static $db = null;

        if ($db === null) {
            //USE DEV PROPERTIES if debug is on.
            if(Config::DEBUG){
                $dsn = 'mysql:host=' . Config::DEV_DB_HOST . ';dbname=' . Config::DEV_DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, Config::DEV_DB_USER, Config::DEV_DB_PASSWORD);
            }else{
                $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);
            }
            // Throw an Exception when an error occurs
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $db;
    }
}
