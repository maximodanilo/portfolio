<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Sessions
 */
session_start();


/**
 * Routing
 */
$router = new Core\Router();

//$pattern = "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]+)$";

// Add the routes
$router->add('', ['controller' => 'Home', 'action' => 'index']);
$router->add('login', ['controller' => 'Login', 'action' => 'new']);
$router->add('logout', ['controller' => 'Login', 'action' => 'destroy']);
$router->add('password/reset/{token:[\da-f]+}', ['controller' => 'Password', 'action' => 'reset']);
$router->add('signup/activate/{token:[\da-f]+}', ['controller' => 'Signup', 'action' => 'activate']);

$router->add('customization', ['controller' => 'Customization', 'action' => 'show']);
$router->add('customization/create/{image:[profile]+}', ['controller' => 'Customization', 'action' => 'create']);
$router->add('customization/create/{image:[background]+}', ['controller' => 'Customization', 'action' => 'create']);

$router->add('resume/{version:[v][\d]+}', ['controller' => 'Resume', 'action' => 'show']);
$router->add('resume', ['controller' => 'Resume', 'action' => 'showDefaultVersion']);
$router->add('resume/new', ['controller' => 'Resume', 'action' => 'new']);
$router->add('resume/edit/{id:[\d]+}', ['controller' => 'Resume', 'action' => 'edit']);
$router->add('resume/update/{id:[\d]+}', ['controller' => 'Resume', 'action' => 'update']);
$router->add('resume/delete/{id:[\d]+}', ['controller' => 'Resume', 'action' => 'delete']);
$router->add('resume/create', ['controller' => 'Resume', 'action' => 'create']);

$router->add('resumesection', ['controller' => 'ResumeSection', 'action' => 'show']);
$router->add('resume/{resumeid:[\d]+}/resumesection/new', ['controller' => 'ResumeSection', 'action' => 'new']);
$router->add('resumesection/edit/{id:[\d]+}', ['controller' => 'ResumeSection', 'action' => 'edit']);
$router->add('resumesection/update/{id:[\d]+}', ['controller' => 'ResumeSection', 'action' => 'update']);
$router->add('resumesection/delete/{id:[\d]+}', ['controller' => 'ResumeSection', 'action' => 'delete']);
$router->add('resume/{resumeid:[\d]+}/resumesection/create', ['controller' => 'ResumeSection', 'action' => 'create']);

$router->add('resumesectionitem', ['controller' => 'ResumeSectionitem', 'action' => 'show']);
$router->add('resume/{resumeid:[\d]+}/resumesection/{resumesectionid:[\d]+}/resumesectionitem/new', ['controller' => 'ResumeSectionItem', 'action' => 'new']);
$router->add('resumesectionitem/edit/{id:[\d]+}', ['controller' => 'ResumeSectionItem', 'action' => 'edit']);
$router->add('resumesectionitem/update/{id:[\d]+}', ['controller' => 'ResumeSectionItem', 'action' => 'update']);
$router->add('resumesectionitem/delete/{id:[\d]+}', ['controller' => 'ResumeSectionItem', 'action' => 'delete']);
$router->add('resume/{resumeid:[\d]+}/resumesection/{resumesectionid:[\d]+}/resumesectionitem/create', ['controller' => 'ResumeSectionItem', 'action' => 'create']);


$router->add('profile', ['controller' => 'Profile', 'action' => 'show']);
$router->add('profile/new', ['controller' => 'Profile', 'action' => 'new']);
$router->add('profile/edit/{id:[\d]+}', ['controller' => 'Profile', 'action' => 'edit']);
$router->add('profile/update/{id:[\d]+}', ['controller' => 'Profile', 'action' => 'update']);
$router->add('profile/delete/{id:[\d]+}', ['controller' => 'Profile', 'action' => 'delete']);
$router->add('profile/create', ['controller' => 'Profile', 'action' => 'create']);

$router->add('{controller}/{action}');

$router->dispatch($_SERVER['QUERY_STRING']);
