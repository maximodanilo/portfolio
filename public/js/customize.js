$(document).ready(function (e) {
    
// $("#form").on('submit',(function(e) {
//  e.preventDefault();
//  console.log("uploading img");
//  
//  $.ajax({
//        url: "/customization/create",
//        type: "POST",
//        data:  new FormData(this),
//        contentType: false,
//        cache: false,
//        processData:false,
//        beforeSend : function(){
//            //$("#preview").fadeOut();
//            console.log("before");
//            $("#err").fadeOut();
//        },
//        success: function(data){
//            if(data==='invalid')
//            {
//             // invalid file format.
//             console.log("Invalid File ");
//             $("#err").html("Invalid File !").fadeIn();
//            }
//            else
//            {
//             // view uploaded file.
//             console.log("view uploaded file. " );
//             $
//             $("#form")[0].reset(); 
//            }
//        },
//        error: function(e) {
//            console.log(e);
//        }          
//    });
// }));
    
    $('#uploadBackgroundBtn').hide();    
    $('#uploadProfileBtn').hide(); 
    
});

function f_previewBackground(input) {
    console.log("here" + input.files[0]);
   if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#previewBackground').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        $('#uploadBackgroundBtn').show().fadeIn(2);
    }
}

function f_previewProfile(input) {
    console.log("here" + input.files[0]);
   if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#previewProfile').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        $('#uploadProfileBtn').show().fadeIn(2);
    }
}

function customizeColor(){
    // #XXXXXX -> ["XX", "XX", "XX"]
    var value = $("#color").val();
    value = value.match(/[A-Za-z0-9]{2}/g);

// ["XX", "XX", "XX"] -> [n, n, n]
    value = value.map(function (v) {
        return parseInt(v, 16)
    });

// [n, n, n] -> rgb(n,n,n)
    console.log("rgb(" + value.join(",") + ", 0.38" + ")");
    $(".site-color").css("background-color", "rgb(" + value.join(",") + ", 0.38" + ")");
}

